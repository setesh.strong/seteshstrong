- 👋 Hi, I’m @seteshstrong
- 👀 I’m interested in FOSS communities, intelligence management, and operations and supply chain management.
- 🌱 I’m currently learning infrastructural operations, competitive intelligence, and stakeholder facilitation.
- 💞️ I’m looking to collaborate on FreeBSD community development, generative agent development, and SME incubation.
- 📫 How to reach me... you can likely find me at my gmail address, with a dot delimiter between my first and last name.

<!---
seteshstrong/seteshstrong is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
